## Geowox

Display properties on map to be filtered by some attributes like:
- Property Types
- Beds
- Baths

### Setup and Running
1. Clone project
2. `yarn install`
3. `yarn start`
4. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
