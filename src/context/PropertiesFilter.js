import React, { useEffect, useState } from 'react';

import { filterProperties, getProperties } from '../api/properties';

export const PropertyFilterContext = React.createContext();

export function PropertyFilterProvider ({ children }) {
  const [state, setState] = useState({ list: [], total: 0 });

  useEffect(() => {
    getProperties()
      .then(({ total, data }) => {
        setState({
          total,
          list: data,
        });
      });
  }, []);

  const dispatchFilterProperties = ({ beds, baths, propertyType }) => {
    filterProperties({
      beds: Number(beds),
      baths: Number(baths),
      propertyType,
    })
      .then(({ data }) => {
        setState({
          ...state,
          list: data,
        });
      });
  }

  return (
    <PropertyFilterContext.Provider value={[state, dispatchFilterProperties]}>
      {children}
    </PropertyFilterContext.Provider>
  );
}
