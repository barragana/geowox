import React, { useMemo, useState } from 'react';

import properties from '../__mockData__/properties.json';

export const PropertyDetailsContext = React.createContext();

export function PropertyDetailsProvider ({ children }) {
  const [data, setData] = useState();
  const [view, setView] = useState(false);

  const findPropertyById = propertyId => {
    const property = properties.data.find(({ id }) => propertyId === id );
    setData(property);
  }

  const value = useMemo(() =>
    [data, findPropertyById, view, setView],
    [data, view],
  );

  return (
    <PropertyDetailsContext.Provider value={value}>
      {children}
    </PropertyDetailsContext.Provider>
  );
}
