const generateNumberOptions = (from, to) => Array.from(Array(to).keys()).map(value => ({ value: value+from, label: value+from }));

export const propertyTypesOptions = [
  { "value": "TERRACED", "label": "Terraced" },
  { "value": "DETACHED", "label": "Detached" },
  { "value": "SEMI-DETACHED", "label": "Semi-Detached" },
  { "value": "APARTMENT", "label": "Apartment" }
];

export const bedroomsOptions = generateNumberOptions(1, 7);
export const bathroomsOptions = generateNumberOptions(1, 7);

export const propertyColorsByType = {
  TERRACED: 'green',
  DETACHED: 'yellow',
  'SEMI-DETACHED': 'red',
  APARTMENT: 'blue',
};
