import React, { useContext } from 'react';

import PropertyDetails from '../components/PropertyDetails';
import Map from '../components/Map/Map';

import { PropertyFilterContext } from '../context/PropertiesFilter';
import { PropertyDetailsContext } from '../context/PropertyDetails';

import './details.css';

function Details() {
  const [
    propertyDetails,
    setPropertyDetails,
    view,
    setView,
  ] = useContext(PropertyDetailsContext);
  const [filteredProperties] = useContext(PropertyFilterContext);

  const handleMarkerClick = propertyId => {
    setPropertyDetails(propertyId);
    setView(true);
  };

  return (
    <div className="details">
      <PropertyDetails
        hidden={!view}
        onClose={() => { setView(false); }}
        {...propertyDetails}
      />
      <Map
        onMarkerClick={handleMarkerClick}
        properties={filteredProperties.list}
      />
    </div>
  );
}

export default Details;
