import React, { useContext, useState } from 'react';

import Select from '../components/Select';
import PieChart from '../components/PieChart';
import Label from '../components/Label';

import { PropertyFilterContext } from '../context/PropertiesFilter';

import {
  bathroomsOptions,
  bedroomsOptions,
  propertyTypesOptions
} from '../presents';
import { classNameIfNoValue, optionsWithPlaceholder } from '../utils';

import './header.css';

function Header() {
  const [state, setState] = useState({
    beds: '',
    baths: '',
    propertyType: ''
  });

  const [{ list, total }, filterProperties] = useContext(PropertyFilterContext);

  const onFilterChange = (attrName, value) => {
    const nextState = {...state, [attrName]: value };
    setState(nextState);
    filterProperties(nextState);
  }

  return (
    <div className="header">
      <div className="header__filters__header">
        <Label bold>Filters</Label>
      </div>
      <div className="header__stats__header">
        <Label bold>Property Type</Label>
      </div>
      <div className="header__select__container">
        <Label
          size="s"
          classNames={classNameIfNoValue(state.propertyType, 'label--hidden')}
        >
          Property Type
        </Label>
        <Select
          name="propertyType"
          options={
            optionsWithPlaceholder('Property Type',propertyTypesOptions)
          }
          selectedValue={state.propertyType}
          onChange={onFilterChange}
        />
      </div>
      <div className="header__select__container">
        <Label
          size="s"
          classNames={classNameIfNoValue(state.beds, 'label--hidden')}
        >
          Beds
        </Label>
        <Select
          name="beds"
          options={optionsWithPlaceholder('Bedrooms', bedroomsOptions)}
          selectedValue={state.beds}
          onChange={onFilterChange}
        />
      </div>
      <div className="header__select__container">
        <Label
          size="s"
          classNames={classNameIfNoValue(state.baths, 'label--hidden')}
        >
          Baths
        </Label>
        <Select
          name="baths"
          options={optionsWithPlaceholder('Bathrooms', bathroomsOptions)}
          selectedValue={state.baths}
          onChange={onFilterChange}
        />
      </div>
      <div className="header__stats__container">
        <PieChart data={[list.length, (total - list.length)]} />
      </div>
    </div>
  );
}

export default Header;
