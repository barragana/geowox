import properties from '../__mockData__/properties.json';

/**
 * Get all properties list
 */
export function getProperties () {
  return Promise.resolve(properties);
}

/**
 * Filter properties by beds, baths and propertyTypes if exists
 * @param {Number} beds Number o beds, if empty or null discard
 * @param {Number} baths Number o baths, if empty or null discard
 * @param {String} propertyType One of [TERRACE, SEMI-DETACHED, DETACHED, APARTMENT]
 */
export function filterProperties ({ beds, baths, propertyType }) {
  return Promise.resolve({
    data: properties.data.filter(property => (
      (!beds || property.beds === beds) &&
      (!baths || property.baths === baths) &&
      (!propertyType || property.propertyType === propertyType)
    ))
  });
}
