import React from 'react';
import PropTypes from 'prop-types';

import Text from './Text';

function Currency({ locale, currency, children, ...props }) {
  return (
    <Text {...props}>
      {new Intl.NumberFormat(
        locale, { style: 'currency', currency }
      ).format(children)}
    </Text>
  )
}

Currency.propTypes = {
  children: PropTypes.number.isRequired,
}

Currency.defaultProps = {
  locale: 'en-IE',
  currency: 'EUR',
}

export default Currency;
