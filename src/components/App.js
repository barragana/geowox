import React from 'react';

import Header from '../containers/Header';
import Details from '../containers/Details';

import { PropertyDetailsProvider } from '../context/PropertyDetails';
import { PropertyFilterProvider } from '../context/PropertiesFilter';

import './app.css';

function App() {
  return (
    <PropertyFilterProvider>
      <div className="app__layout">
        <Header />
        <PropertyDetailsProvider>
          <Details />
        </PropertyDetailsProvider>
      </div>
    </PropertyFilterProvider>
  );
}

export default App;
