import React, { useState, useEffect, memo } from 'react';
import { Map, Marker, GoogleApiWrapper } from 'google-maps-react';
import { generateBounds } from './utils';

import './map.css';
import { propertyColorsByType } from '../../presents';

const mapStyles = {
  container: {
    position: 'relative',
    width: '100%',
    height: '100%',
  },
};

function MapContainer ({ google, onMarkerClick, properties, mapPosition }) {
  const [bounds, setBounds] = useState();

  useEffect(() => {
    const points = properties.map(({ lat, lng }) => ({ lat, lng }));
    setBounds(generateBounds(points, google));
  }, [google, properties])

  const positionProps = properties.length ? { bounds } : mapPosition;
  return (
    <Map
      google={google}
      containerStyle={mapStyles.container}
      {...positionProps}
    >
      {
        properties.map(({ lat, lng, propertyType, id }) => (
          <Marker
            key={id}
            onClick={() => onMarkerClick(id)}
            position={{ lat, lng }}
            name={propertyType}
            icon={{
              url: `http://maps.google.com/mapfiles/ms/icons/${propertyColorsByType[propertyType]}-dot.png`,
            }}
          />
        ))
      }
    </Map>
  );
}

const LoadingContainer = () => (
  <div className="map__loading-container">Loading Google Maps...</div>
)

export default memo(
  GoogleApiWrapper({
    apiKey: (process.env.REACT_APP_GOOGLE_MAPS_API_KEY),
    LoadingContainer,
  })(MapContainer),
  (prevProps, nextProps) => (
    prevProps.google === nextProps.google &&
    prevProps.properties === nextProps.properties
  ),
);

MapContainer.defaultProps = {
  mapPosition: {
    center: {
      lat: 53.248862,
      lng: -6.12526,
    },
    zoom: 12,
  }
};
