export const generateBounds = (points, google) => {
  const bounds = new google.maps.LatLngBounds();
  for (let i = 0; i < points.length; i++) {
    bounds.extend(points[i]);
  }
  return bounds;
}
