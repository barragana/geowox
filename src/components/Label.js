import React from 'react';
import PropTypes from 'prop-types';

import ctx from 'classnames';

import './label.css';

const Label = ({ children, bold, size, classNames, ...props }) => (
  <span
    className={
      ctx(
        'label',
        bold && 'label--bold',
        (size === 's' && 'label--s') ||
          (size === 'l' && 'label--l') ||
          'label--m',
        classNames,
      )}
    {...props}
  >
    {children}
  </span>
);

Label.propTypes = {
  size: PropTypes.oneOf(['s', 'm', 'l']),
  classNames: PropTypes.arrayOf(PropTypes.string),
  bold: PropTypes.bool,
  children: PropTypes.node.isRequired,
}

export default Label;
