import React from 'react';
import ctx from 'classnames';

import Currency from './Currency';
import Label from './Label';
import Text from './Text';

import { textValueOrDash } from '../utils';

import './propertyDetails.css';

function PropertyDetails ({ hidden, onClose, ...props }) {
  return (
    <div
      className={
        ctx(
          'property-details__container',
          hidden && 'property-details--hidden'
        )
    }>
      <button
        className="property-details__close-button"
        onClick={onClose}
      >
        X
      </button>
      <div className="property-details__fieldset">
        <h4 className="property-details__header">Property Details</h4>
        <div className="property-details__field-group--double">
          <Label>Beds</Label>
          <Text className="property-details__value">{props.beds || '-'}</Text>
        </div>
        <div className="property-details__field-group--double">
          <Label>Baths</Label>
          <Text>{textValueOrDash(props.baths)}</Text>
        </div>
        <div className="property-details__field-group--single">
          <Label>Property Type</Label>
          <Text>{textValueOrDash(props.propertyType)}</Text>
        </div>
        <div className="property-details__field-group--single">
          <Label>Sqm</Label>
          <Text>{textValueOrDash(props.sqm)}</Text>
        </div>
        <div className="property-details__field-group--single">
          <Label>Address</Label>
          <Text>{textValueOrDash(props.address)}</Text>
        </div>
        <div className="property-details__field-group--single">
          <Label>Price</Label>
          <Text>
            {(props.price && <Currency>{props.price}</Currency>) || '-'}
          </Text>
        </div>
      </div>
    </div>
  )
}

export default PropertyDetails;
