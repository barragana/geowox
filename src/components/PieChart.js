import React, { useEffect, useRef } from 'react'
import * as d3 from 'd3'

function drawPieChart(canvasRef, data) {
  const width = 45
  const height = 45
  const margin = 4

  const radius = (Math.min(width, height) / 2 - margin);

  (canvasRef.current.firstChild && canvasRef.current.firstChild.remove());
  const svg = d3.select(canvasRef.current)
    .append("svg")
      .attr("width", width)
      .attr("height", height)
      .append("g")
      .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

  const color = d3.scaleOrdinal(['white', 'grey']);

  const pie = d3.pie().sort(null)(data);

  svg
    .selectAll('react')
    .data(pie)
    .enter()
    .append('path')
    .attr('d', d3.arc()
      .innerRadius(radius)
      .outerRadius(0)
    )
    .attr('fill', function(d, i){ return(color(i)) })
    .attr("stroke", "black")
    .style("stroke-width", "1px")
}

function PieChart({ data }) {
  const canvasRef = useRef();

  useEffect(() => {
    drawPieChart(canvasRef, data);
  }, [data]);

  return (
    <div ref={canvasRef}></div>
  );
}

export default PieChart
