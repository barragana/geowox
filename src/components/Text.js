import React from 'react';
import './text.css';

const Text = ({ children }) => (
  <span className="text">{children}</span>
);

export default Text;
