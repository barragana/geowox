import React from 'react';
import ctx from 'classnames';

import './select.css';
import { classNameIfNoValue } from '../utils';

function Select({ name, options, onChange, selectedValue }) {
  const handleOnChange = (e) => {
    onChange(e.target.name, e.target.value);
  };

  return (
    <select
      className={
        ctx(
          'select',
          classNameIfNoValue(selectedValue, 'select--placeholder'),
        )
      }
      name={name}
      onChange={handleOnChange}
    >
      {options.map(({ value, label }) => (
        <option key={value} value={value}>{label}</option>
      ))}
    </select>
  );
}

export default Select;
