export const textValueOrDash = value => value || '-';
export const optionsWithPlaceholder = (placeholder, options) => (
  [{ label: placeholder, value: '' }].concat(options)
);
export const classNameIfNoValue = (value, className) => (!value && className);
